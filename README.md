# Endpoint de chistes

Prueba tecnica Back-End Suazo.com

El archivo [API_Rest.py](https://gitlab.com/luisqpra/endpoint-de-chistes/-/blob/main/API_Rest.py) contiene los Endpoint de la prueba.

Para iniciar el servidor se debe ejecutar el siguiente comando en la terminal:

` python API_Rest.py`

Utiliza el puerto 4000, si se requiere cambiar el puerto se debe cambiar la siguiente linea de codigo dentro de [API_Rest.py](https://gitlab.com/luisqpra/endpoint-de-chistes/-/blob/main/API_Rest.py), que se encuentra al final del codigo.

`   app.run(debug=True, port=4000) # Puerto 4000`

## Endpoints

- Para consumir  el api de chiste aleatorio es con la URL http://localhost:4000/chistes/
- Para consumir el api de chiste Dad o Chuck es con la URL http://localhost:4000/chistes/path, donde path puede ser "Chuck" o "Dad", si se pone otra opcion se devuelve un 404. Chuck http://localhost:4000/chistes/Chuck. Dad http://localhost:4000/chistes/Dad.
- Para consumir el api de  mínimo común múltiplo es con la URL http://localhost:4000/matematico_1, donde se debe agregar un query param con la lista de numeros, siendo que los numeros son enteros y solo esten separados por comas. Como por ejemplo http://localhost:4000/matematico_1?numbers=3,6,7
- Para consumir el api de mas 1  es con la URL http://localhost:4000/matematico_2 , donde se debe agregar un query param, ingresando solo un valor numerico entero. Como  por ejemplo  http://localhost:4000/matematico_2?numbers=3.

*Nota: si se ha cambiado el numero del puerto, debe poner el mismo valor en las URL's 


