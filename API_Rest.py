# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 16:19:37 2021

@author: Luis Quiroz
"""

from flask import Flask, render_template, request
import requests 
from random import choice
import numpy as np
import math
app = Flask(__name__)

#Consumir API Chuck
def chiste_chuck():
    URL = 'https://api.chucknorris.io/jokes/random'
    data = requests.get(URL)
    dataj = data.json() 
    return({"Chiste": dataj['value']})

#Consumir API Dad
def chiste_Dad():
    URL =  'https://icanhazdadjoke.com/'
    data = requests.get(URL,  headers= {"Accept":"text/plain"})
    return({"Chiste":data.text})

# rutina get chistes sin path
#http://localhost:4000/chistes/
@app.route('/chistes/', methods=['GET'])
def chistes_1():    
    return(choice([chiste_chuck,chiste_Dad])())

# rutina get chistes con path
#http://localhost:4000/chistes/Chuck
#http://localhost:4000/chistes/Dad
@app.route('/chistes/<seccion>', methods=['GET'])
def chistes_2(seccion):
    if seccion == 'Chuck':
        return(chiste_chuck())
    elif seccion == 'Dad':
        return(chiste_Dad())
    else:
        return (render_template('404.html'), 404)
    
# Funcion  minimo comun divisorx2
def mcd2(x, y):
    mayor = max(x, y)
    menor = min(x, y)
    if ((x and y) == 0):
       menor = mayor
    else:
        r = mayor % menor
        while r != 0:
            mayor = r
            r = menor%r
            menor = mayor
    return math.fabs(menor)

# Funcion  minimo comun divisor
def mcd(n):
    numeros = n
    resultado = mcd2(numeros[0], numeros[1])
    if len(numeros) > 2:
        for n in numeros[2:]:
            resultado = mcd2(resultado, n)
    return math.fabs(resultado)

# Funcion  minimo comun multiplo
def MCM(n):
    numeros = n
    if np.prod(numeros) != 0:
        resultado = np.prod(numeros)/mcd(n)**(len(numeros) - 1)
    else:
        resultado = float('nan')
    return math.fabs(resultado)        
    
#rutina minimo comun multiplo
#http://localhost:4000/matematico_1?numbers=3,6,7
@app.route('/matematico_1', methods=['GET'])
def matematico_1():
    numbers=request.args.get('numbers').split (',') 
    numbers=[int(n) for n in numbers]
    
    return({'numero':str(MCM(numbers))}) 

#rutina sumar 1
#http://localhost:4000/matematico_2?numbers=3
@app.route('/matematico_2', methods=['GET'])
def matematico_2():
    numbers=request.args.get('numbers')
    numbers=int(numbers)+1
    return({'numero':str(numbers)})    
    
if __name__ == '__main__':
    app.run(debug=True, port=4000)
